/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polimorfismo.en.java;

/**
 *
 * @author JULIAN
 */
public class PolimorfismoEnJava {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Carro c = new CarroNascar();
		
		c.informacion();
		
		Carro ct = new CarroFord();
		
		ct.informacion();
		
	}

}
