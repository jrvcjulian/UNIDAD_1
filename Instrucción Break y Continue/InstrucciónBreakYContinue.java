/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package instrucción.pkgbreak.y.pkgcontinue;

/**
 *
 * @author JULIAN
 */
public class InstrucciónBreakYContinue {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
		for (int i = 0; i <= 20; i++) {
			System.out.println("Aun estas en el ciclo");
			if (i==9) {
				break; // se sale del ciclo completamente
				//continue;  // ignora lo siguiente y empieza desde arriba
			}
			System.out.println("El valor de i es: "+i);
		}
		
		System.out.println("Has dejado el ciclo for");
	}

}
