/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia.en.java;

/**
 *
 * @author JULIAN
 */
public class Carro {

	 double peso;
	 double altura;
	 double ancho;
	 double largo;
	 
	 int numeroPuertas;
	 boolean encendido=false;
	 String modelo;
	 
	 public Carro()
	 {
		 this.peso=1000;
		 this.altura=1.90;
		 this.ancho=2;
	 }
	 
	 public double ObtenerPeso()
	 {
	     return this.peso;
	 }

	 public void encender()
	 {
	     this.encendido=true;  
		 System.out.println("El carro esta encendido");
	       
     }
	 
	 public void apagar()
	 {
	     this.encendido=false;  
		 System.out.println("El carro esta apagado");
	       
	 }
	
	 public void estado()
	 {
		 if(this.encendido==true)
			 System.out.println("El carro esta encendido");
		 else
			 System.out.println("El carro esta apagado");
	 }
		 
}

class CarroNascar extends Carro
{
	public CarroNascar()
	{
		this.modelo="NASCAR";
	}
	
	public void turbo()
	{
		System.out.println("Velocidad a mas de 160 millas por horas");
	}
	
}

class CarroFord extends Carro
{
	public CarroFord()
	{
		this.modelo="FORD";
	}
	
	public String modelo()
	{
		return this.modelo;
	}
}
