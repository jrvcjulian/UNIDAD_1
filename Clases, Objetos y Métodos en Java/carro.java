/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases.objetos.y.métodos.en.java;

/**
 *
 * @author JULIAN
 */
public class carro {

	 double peso;
	 double altura;
	 double ancho;
	 double largo;
	 
	 int numeroPuertas;
	 boolean encendido=false;
	 String modelo;
	 
	 public carro()
	 {
		 this.peso=1000;
		 this.altura=1.90;
		 this.ancho=2;
	 }
	 
	 public double ObtenerPeso()
	 {
	     return this.peso;
	 }

	 public void encender()
	 {
	     this.encendido=true;  
		 System.out.println("El carro esta encendido");
	       
     }
	 
	 public void apagar()
	 {
	     this.encendido=false;  
		 System.out.println("El carro esta apagado");
	       
	 }
	
	 public void estado()
	 {
		 if(this.encendido==true)
			 System.out.println("El carro esta encendido");
		 else
			 System.out.println("El carro esta apagado");
	 }	 
}
